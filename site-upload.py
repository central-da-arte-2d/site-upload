#!/usr/bin/python3
# encoding: utf-8
import sys
from saucenao_api import SauceNao
import cutlet
import time
import os
import subprocess
import readline
import requests
import io
import pathlib
from PIL import Image
current_dir = pathlib.Path(__file__).resolve().parent
dic_path = str(current_dir) + '/author-dic'
temp_path = str(current_dir) + '/temp'
thumb_width = 252
thumb_height = 315
sauce = SauceNao('dafe625cf5b2e14ceba7a772957220fbeb0702fd')
filepath = sys.argv[1]
filepath = pathlib.Path(filepath).resolve()

def rlinput(prompt, prefill=''):
   readline.set_startup_hook(lambda: readline.insert_text(prefill))
   try:
      return input(prompt)  # or raw_input in Python 2
   finally:
      readline.set_startup_hook()

def check_if_cjk(string):
	ranges = [
	  {"from": ord(u"\u3300"), "to": ord(u"\u33ff")},         # compatibility ideographs
	  {"from": ord(u"\ufe30"), "to": ord(u"\ufe4f")},         # compatibility ideographs
	  {"from": ord(u"\uf900"), "to": ord(u"\ufaff")},         # compatibility ideographs
	  {"from": ord(u"\U0002F800"), "to": ord(u"\U0002fa1f")}, # compatibility ideographs
	  {'from': ord(u'\u3040'), 'to': ord(u'\u309f')},         # Japanese Hiragana
	  {"from": ord(u"\u30a0"), "to": ord(u"\u30ff")},         # Japanese Katakana
	  {"from": ord(u"\u2e80"), "to": ord(u"\u2eff")},         # cjk radicals supplement
	  {"from": ord(u"\u4e00"), "to": ord(u"\u9fff")},
	  {"from": ord(u"\u3400"), "to": ord(u"\u4dbf")},
	  {"from": ord(u"\U00020000"), "to": ord(u"\U0002a6df")},
	  {"from": ord(u"\U0002a700"), "to": ord(u"\U0002b73f")},
	  {"from": ord(u"\U0002b740"), "to": ord(u"\U0002b81f")},
	  {"from": ord(u"\U0002b820"), "to": ord(u"\U0002ceaf")}  # included as of Unicode 8.0
	]

	def is_cjk(char):
	  return any([range["from"] <= ord(char) <= range["to"] for range in ranges])
	cjk = False
	i = 0
	while i<len(string):
	  if is_cjk(string[i]):
	    cjk = True
	  i += 1
	return cjk

file = open(filepath, 'rb')
results = sauce.from_file(file)

pixiv_resp_id = ''
result = ''
best_similarity = results[0].similarity
for i, res in enumerate(results):
  if res.raw["header"]["index_id"] == 5 or res.raw["header"]["index_id"] == 6:
    #esse é o índice do resultado do Pixiv
    pixiv_resp_id = i
    break

if pixiv_resp_id == '':
  print ('Não encontrado no Pixiv.')
  result = results[0]
else:
  pixiv_similarity = results[pixiv_resp_id].similarity
  if int(pixiv_similarity) < 90:
    print('Baixa similaridade no Pixiv ('+ str(pixiv_similarity) + '%)')
    print('Resultado com maior similaridade('+ str(best_similarity) + '%): ' + results[0].raw["header"]["index_name"])
    result = results[0]
  else:
    print('Alta similaridade no Pixiv ('+ str(pixiv_similarity) + '%)')
    result = results[pixiv_resp_id]

print('')
file.close()

if result.author is None:
  author = ''
else:
  author = result.author

fullAuthor = ""

#mostra links
for url in result.urls:
  print(url)
print('')

#mostra miniatura
thumb = result.thumbnail
r = requests.get(thumb)
with open(temp_path + '/thumb.jpg', 'wb') as fi:
  fi.write(r.content)
img = subprocess.call(['imgcat', temp_path + '/thumb.jpg'])

#procura author no dicionário
dic_authors = {}
with io.open(dic_path, "r", encoding="utf-8") as f:
  dic_authors = dict(i.rstrip().split("\t", 1) for i in f)

if  author.lower() in dic_authors:
  dic = True
  print('Encontrado no dicionário, utilizando nome registrado...')
  fullAuthor = dic_authors[author.lower()]
else:
  dic = False
  print('Não encontrado no dicionário, formatando nome...')
  fullAuthor = ""
  if check_if_cjk(author):
    katsu = cutlet.Cutlet('hepburn')
    romaji = katsu.romaji(author)
    romajif = ""
    split = romaji.split()
    i = 0
    for i, name in enumerate(split):
       if i > 0:
          romajif += " "
       if name.isupper():
          romajif += name
       else:
          romajif += name.capitalize()

    fullAuthor = author + ' ('+romajif+')'
  else:
    fullAuthor = author.title()
print('')
fullAuthor_temp = rlinput("Artista: ", fullAuthor)

if fullAuthor_temp != fullAuthor:
  fullAuthor = fullAuthor_temp
  if not dic and author != '':
    print('Você alterou o nome, deseja adicionar ao dicionário? ([y] para "sim")')
    adicionar_dic = input('-> ')
    if adicionar_dic == 'y':
      with open(dic_path, 'a') as f:
        f.write(f'{author.lower()}\t{fullAuthor}\n')
        print('Adicionado ao dicionário.')

print('')

with Image.open(filepath) as img:
  src_width, src_height = img.size
imAR = src_width / src_height
zcAR = thumb_width / thumb_height
sideX = round(src_height * zcAR)
sideY = round(thumb_width / zcAR)
thumbnailH = round(max(sideY, sideY * zcAR / imAR))

gravities = {
  'L': 'west',
  'R': 'east',
  'T': 'north',
  'B': 'south',
  'TL': 'northwest',
  'TR': 'northeast',
  'BL': 'southwest',
  'BR': 'southeast'
}
confirm = ''
while confirm != 'y':
  print('Como quer croppar a thumb?')
  print('([L],[R],[T],[B],[TL],[TR],[BL],[BR], vazio para centralizar)')
  crop = input('-> ')
  gravity = gravities.get(crop.upper(),'center')
  subprocess.run(['convert', filepath, '-density','150', '-background', '#FFFFFF', '-thumbnail', 'x'+str(thumbnailH), '-gravity', gravity, '-crop', '252x315+0+0', '+repage', '-quality', '90', '-interlace', 'line' , 'jpeg:' + temp_path + '/thumb_mgk.jpg'])
  subprocess.call(['imgcat', temp_path + '/thumb_mgk.jpg'])
  print('Confirmar? [y],[n]')
  confirm = input('-> ')

epoch = int(time.time())
ext = os.path.splitext(filepath)[1]
newfilename = str(epoch) + '__'+ fullAuthor + '__' + crop.upper() + ext
newfilepath = os.path.dirname(filepath) + '/' + newfilename

os.rename(filepath, newfilepath)
print('')
opcao_pasta = ''
while opcao_pasta != '1'  and opcao_pasta != '2':
  print('Para qual categoria?')
  print('(1) Para G')
  print('(2) Para R-18')
  opcao_pasta = input('-> ')
  print('')

pasta = ''
if opcao_pasta == '2':
  pasta = 'ilustsR18'
else:
  pasta = 'ilusts'

print('Você escolheu: '+ pasta)
print('')
srvpath = '/home/koko/srv/htdocs/' + pasta + '/' + newfilename
print(srvpath)
p = subprocess.Popen(['scp',newfilepath,'koko@192.168.100.6:'+'"'+srvpath+'"'])

input("Ok...")
